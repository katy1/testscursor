from page.PageMain import PageMain


class MethodMain(object):
    page_main = PageMain()

    def authorization(self, driver):
        self.page_main.click_button_signin(driver)
        self.page_main.set_value_login(driver, "katy_gubanova@mail.ru")
        self.page_main.set_value_password(driver, "123456")
        self.page_main.click_button_auth(driver)
