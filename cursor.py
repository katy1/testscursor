from selenium import webdriver
import unittest
from page.PageMenu import PageMenu
from page.PageMain import PageMain
from MethodMain import MethodMain


class TestCase(unittest.TestCase):
    def setUp(self):
        self.page_menu = PageMenu()
        self.page_main = PageMain()
        self.page_method_main = MethodMain()
        self.driver = webdriver.Firefox()

    def test_cursor(self):
        driver = self.driver
        driver.get("http://way2automation.com/way2auto_jquery/menu.php")
        driver.switch_to_window(driver.window_handles[-1])
        self.page_method_main.authorization(self.driver)
        self.driver.refresh()
        self.page_main.click_menu_widget(driver)
        self.page_main.click_submenu(driver)
        self.page_menu.click_menu_with_submenu(driver)
        self.page_menu.set_frame(driver)
        adams = self.page_menu.cursor_submenu(driver)
        class_focus = adams.get_attribute("class")
        self.assertEquals(class_focus, "ui-menu-item ui-state-focus")

    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
