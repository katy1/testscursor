# -*- coding: utf8 -*-
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains


class PageMenu(object):
    _locators = {
        'menu_with_submenu': "//*[@class='responsive-tabs']/li[2]/a",
        'frame': "//*[@id='example-1-tab-2']//iframe",
        'adams': "//*[@id='menu']/li[@id='ui-id-1']"
    }

    def __find_button(self, driver, path_element):
        WebDriverWait(driver, 10).until(EC.element_to_be_clickable((
            By.XPATH, path_element)))
        return driver.find_element_by_xpath(path_element)

    def __find_element(self, driver, path_element):
        WebDriverWait(driver, 10).until(EC.presence_of_element_located((
            By.XPATH, path_element)))
        return driver.find_element_by_xpath(path_element)

    def click_menu_with_submenu(self, driver):
        self.__find_button(
            driver, self._locators['menu_with_submenu']).click()

    def set_frame(self, driver):
        frame = self.__find_element(
            driver, self._locators['frame'])
        driver.switch_to_frame(frame)

    def cursor_submenu(self, driver):
        adams = self.__find_button(
            driver, self._locators['adams'])
        action_chains = ActionChains(driver)
        action_chains.move_to_element(adams).perform()
        return adams
